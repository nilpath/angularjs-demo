(function(){
  'use strict';

  var CheckoutController = function($cookieStore, $rootScope) {

    var checkout = this;

    this.shoppingBag = $cookieStore.get('bag') || [];

    this.updateQty = function($event, item, qty) {
      $event.preventDefault();

      item.qty += qty;

      if(item.qty < 1) {
        removeItem(item);
      }

      $cookieStore.put('bag', checkout.shoppingBag);
      updateCart();
    };

    this.remove = function($event, item) {
      $event.preventDefault();
      
      removeItem(item);
      $cookieStore.put('bag', checkout.shoppingBag);
      updateCart();
    };

    this.shoppingBagHasItems = function() {
      return checkout.shoppingBag.length > 0;
    }

    function removeItem(item) {
      var index = checkout.shoppingBag.indexOf(item);
      checkout.shoppingBag.splice(index, 1);
    }
    
    function updateCart() {
      $rootScope.$broadcast('shoppingBag.update');
    }
  };

  CheckoutController.$inject = ['$cookieStore', '$rootScope'];

  angular
    .module('App.Controllers')
    .controller('CheckoutController', CheckoutController);

})();
