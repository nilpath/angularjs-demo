(function(){
  'use strict';

  var ProductListController = function($cookieStore, $rootScope, ProductsService) {

    this.searchQuery = ' ';
    this.products = ProductsService.getProducts();

    this.addToCart = function($event, product) {
      $event.preventDefault();

      var shoppingBag = $cookieStore.get('bag') || [];
      var productInBag = findProductInBag(product, shoppingBag);

      if(productInBag) {
        productInBag.qty++;
      } else {
        var cartProduct = angular.copy(product);
        cartProduct.qty = 1;
        shoppingBag.push(cartProduct);
      }

      $cookieStore.put('bag', shoppingBag);
      $rootScope.$broadcast('shoppingBag.update');

      function findProductInBag(newProduct, bag) {
        var productInBag;

        angular.forEach(bag, function(item) {
          if(item.id === newProduct.id) {
            productInBag = item;
          }
        });

        return productInBag;
      }

    };

  };

  //Add strings of injected dependencies to survive JS minification
  ProductListController.$inject = ['$cookieStore', '$rootScope', 'ProductsService'];

  angular
    .module('App.Controllers')
    .controller('ProductListController', ProductListController);

})();
