(function(){
  'use strict';

  angular.module('App.Controllers', ['ngCookies']);
  angular.module('App.Services', []);
  angular.module('App.Directives', []);

  angular.module('App', [
    'ngRoute',
    'App.Controllers',
    'App.Services',
    'App.Directives'
  ])
  .config(['$routeProvider', function($routeProvider) {

    $routeProvider
      .when('/', {
        controller: 'ProductListController',
        controllerAs: 'productList',
        templateUrl: 'views/product-list.html',
        resolve: {
          products: function(ProductsService) {
            return ProductsService.promise;
          }
        }
      })
      .when('/checkout', {
        controller: 'CheckoutController',
        controllerAs: 'checkout',
        templateUrl: 'views/checkout.html'
      });

  }]);

})();
