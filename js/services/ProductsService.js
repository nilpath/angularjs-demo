(function(){
  'use strict';

  var ProductsService = function($http, $q) {
    var deferred = $q.defer();
    var products;

    $http.get('products.json').success(function(data) { 
      products = data;
      deferred.resolve();
    });

    return {
      getProducts: function() {
        return products;
      },
      promise: deferred.promise
    };
  };

  //Add strings of injected dependencies to survive JS minification
  ProductsService.$inject = ['$http', '$q'];

  angular
    .module('App.Services')
    .service('ProductsService', ProductsService);

})();
