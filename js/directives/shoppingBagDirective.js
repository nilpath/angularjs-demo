(function(){
  'use strict';
  
  var ShoppingBagDirective = function($cookieStore) {
    
    
    function getTotalBagValue(bag) {
      var total = 0;
      
      angular.forEach(bag, function(item) {
        total += (item.qty * item.price);
      });
      
      return total;
    }
    
    function getItemsInBag(bag) {
      var items = 0;
      
      angular.forEach(bag, function(item) {
        items += item.qty;
      });
      
      return items;
    }
    
    return {
      scope: {
        
      },
      templateUrl: 'views/directives/shoppingBag.html',
      link: function($scope, $el) {
        
        $scope.openBag = function() {
          $scope.isOpen = true;
        };
        
        $scope.closeBag = function() {
          $scope.isOpen = false;
        };
        
        $scope.$on('shoppingBag.update', function(){
          var bag = $cookieStore.get('bag') || [];
          $scope.bag = bag;
          $scope.totalBagValue = getTotalBagValue(bag);
          $scope.itemsInBag = getItemsInBag(bag);
        });
        
        
        //to init the shopping bag
        $scope.$broadcast('shoppingBag.update');
        
      }
    };
    
  };
  
  ShoppingBagDirective.$inject = ['$cookieStore'];
  
  angular
    .module('App.Directives')
    .directive('shoppingBag', ShoppingBagDirective);
  
})();